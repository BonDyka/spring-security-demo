-- auto-generated definition
create table users
(
    id         bigserial,
    email      varchar(255)                                    not null,
    first_name varchar(50)                                     not null,
    last_name  varchar(100)                                    not null,
    password   varchar(255)                                    not null,
    role       varchar(20) default 'USER'::character varying   not null,
    status     varchar(20) default 'ACTIVE'::character varying not null
);

alter table users
    owner to root;

create unique index table_name_email_uindex
    on users (email);

insert into users (id, email, first_name, last_name, password, role, status)
values (1, 'admin@mail.com', 'Admin', 'Adminov', '$2a$12$S439BT0kWWizu9J4dTp6rOSA6Ix0FjGeOYFfW7cVUBzmZ3rdzk6Si', 'ADMIN', 'ACTIVE')